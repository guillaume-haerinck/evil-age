# Evil Age

Developed with Unreal Engine 5.1. It has been made by [Guillaume HAERINCK](https://guillaumehaerinck.com/) and [Laurine LAFONTAINE](https://laflaurine.github.io/portfolio/), both students at [IMAC](https://www.ingenieur-imac.fr/)

This project use the [Gameplay Ability System](https://docs.unrealengine.com/en-US/InteractiveExperiences/GameplayAbilitySystem/index.html).

## Getting Started

As this project isn't only build with blueprints, you need to compile the C++ code.

You can either use Visual Studio or directly Unreal Engine.

Read more about setting up Visual Studio for Unreal Engine [here](https://docs.unrealengine.com/en-US/ProductionPipelines/DevelopmentSetup/VisualStudioSetup/index.html) and compiling project [here](https://docs.unrealengine.com/en-US/ProductionPipelines/DevelopmentSetup/CompilingProjects/index.html).

With Unreal Engine, you need to click on "Compile".

<p align="center">
    <img src="./doc/compile.jpg"/>
</p>