#pragma once

#include "EvilTypes.generated.h"

// This header is for enums and structs used by classes and blueprints accross the game
// Collecting these in a single header helps avoid problems with recursive header includes

UENUM(BlueprintType)
enum class CharacterType : uint8 {
	UNKNOWN UMETA(DisplayName = "Unknown"),
	VILLAGER UMETA(DisplayName = "Villager"),
	KNIGHT UMETA(DisplayName = "Knight"),
	ENEMY UMETA(DisplayName = "Enemy")
};

UENUM(BlueprintType)
enum class BuildingType : uint8 {
	UNKNOWN UMETA(DisplayName = "Unknown"),
	HOUSE UMETA(DisplayName = "House"),
	RESSOURCE UMETA(DisplayName = "Ressource")
};

UENUM(BlueprintType)
enum class TimeState : uint8 {
	MORNING = 0 UMETA(DisplayName = "Morning"),
	NOON = 1 UMETA(DisplayName = "Noon"),
	AFTERNOON = 2 UMETA(DisplayName = "Afternoon"),
	EVENING = 3 UMETA(DisplayName = "Evening"),
	MIDNIGHT = 4 UMETA(DisplayName = "Midnight"),
	BEFORE_MORNING = 5 UMETA(DisplayName = "Before morning")
};

UENUM(BlueprintType)
enum class EvilCharacterState : uint8 {
	WANDER = 0 UMETA(DisplayName = "Wander"),
	ATTACK = 1 UMETA(DisplayName = "Attack"),
	DEAD = 2 UMETA(DisplayName = "Dead"),
	PURSUIT = 3 UMETA(DisplayName = "Pursuit"),
	IN_BUILDING = 4 UMETA(DisplayName = "In building"),
	DO_TASK = 5 UMETA(DisplayName = "Do task"),
	ESCAPE = 6 UMETA(DisplayName = "Escape")
};

/**
 *
 */
USTRUCT(BlueprintType)
struct FGenome
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float Health;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float AttackPower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float DefensePower;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MoveSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float FieldOfView;
};

/**
 * 
 */
USTRUCT(BlueprintType)
struct FFitness
{
	GENERATED_BODY()

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 AttacksGiven;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	uint8 RessourceGathered;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float SecondsAlive;

	FFitness()
	{
		AttacksGiven = 0;
		SecondsAlive = 0.0f;
		RessourceGathered = 0;
	}
};


USTRUCT(BlueprintType)
struct FCharacterCharacteristics
{
	GENERATED_BODY()

	UPROPERTY()
	FGenome Genome;

	UPROPERTY()
	FFitness Fitness;

	FCharacterCharacteristics() {}
	FCharacterCharacteristics(FGenome Genome, FFitness Fitness)
	{
		Fitness = Fitness;
		Genome = Genome;
	}
};
