// MIT License


#include "Abilities/EvilAttributeSet.h"
#include "GameplayEffect.h"
#include "GameplayEffectExtension.h"
#include "Interfaces/DamageableInterface.h"
#include "Kismet/KismetSystemLibrary.h"
#include "Net/UnrealNetwork.h"

UEvilAttributeSet::UEvilAttributeSet()
	: Health(100.f)
	, MaxHealth(100.f)
	, AttackPower(1.0f)
	, DefensePower(1.0f)
	, MoveSpeed(1.0f)
	, Damage(1.0f)
	, FieldOfView(60.0f)
{

}

void UEvilAttributeSet::GetLifetimeReplicatedProps(TArray<FLifetimeProperty>& OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME_CONDITION_NOTIFY(UEvilAttributeSet, Health, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UEvilAttributeSet, MaxHealth, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UEvilAttributeSet, AttackPower, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UEvilAttributeSet, DefensePower, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UEvilAttributeSet, MoveSpeed, COND_None, REPNOTIFY_Always);
	DOREPLIFETIME_CONDITION_NOTIFY(UEvilAttributeSet, FieldOfView, COND_None, REPNOTIFY_Always);
}

void UEvilAttributeSet::PostGameplayEffectExecute(const FGameplayEffectModCallbackData& Data)
{
	Super::PostGameplayEffectExecute(Data);

	FGameplayEffectContextHandle Context = Data.EffectSpec.GetContext();
	UAbilitySystemComponent* Source = Context.GetOriginalInstigatorAbilitySystemComponent();
	const FGameplayTagContainer& SourceTags = *Data.EffectSpec.CapturedSourceTags.GetAggregatedTags();

	// Get owner
	AActor* TargetActor = nullptr;
	IDamageableInterface* TargetCharacter = nullptr;
	if (Data.Target.AbilityActorInfo.IsValid() && Data.Target.AbilityActorInfo->AvatarActor.IsValid())
	{
		TargetActor = Data.Target.AbilityActorInfo->AvatarActor.Get();
		TargetCharacter = Cast<IDamageableInterface>(TargetActor);
	}

	// Compute the delta between old and new, if it is available
	// If this was additive, store the raw delta value to be passed along later
	float DeltaValue = 0;
	if (Data.EvaluatedData.ModifierOp == EGameplayModOp::Type::Additive)
		DeltaValue = Data.EvaluatedData.Magnitude;

	// Clamp healh and broadcast event
	if (Data.EvaluatedData.Attribute == GetHealthAttribute())
	{
		SetHealth(FMath::Clamp<float>(GetHealth(), 0, GetMaxHealth()));
		if (TargetCharacter)
		{
			// Interface call, strange that we can call it without being a friend class
			TargetCharacter->Execute_OnHealthChanged(TargetActor, DeltaValue, SourceTags);
		}
	}
	else if (Data.EvaluatedData.Attribute == GetDamageAttribute())
	{
		// Get the Source actor
		AActor* SourceActor = nullptr;
		if (Source && Source->AbilityActorInfo.IsValid() && Source->AbilityActorInfo->AvatarActor.IsValid())
		{
			SourceActor = Source->AbilityActorInfo->AvatarActor.Get();
			// Set the causer actor based on context if it's set
			if (Context.GetEffectCauser())
				SourceActor = Context.GetEffectCauser();
		}

		// Store a local copy of the amount of damage done and clear the damage attribute
		const float LocalDamageDone = GetDamage();
		SetDamage(0.f);

		// Try to extract a hit result
		FHitResult HitResult;
		if (Context.GetHitResult())
			HitResult = *Context.GetHitResult();

		if (LocalDamageDone > 0)
		{
			// Apply the health change and then clamp it
			const float OldHealth = GetHealth();
			SetHealth(FMath::Clamp(OldHealth - LocalDamageDone, 0.0f, GetMaxHealth()));

			if (TargetCharacter)
			{
				// Interface call, strange that we can call it without being a friend class
				TargetCharacter->Execute_OnHealthChanged(TargetActor, -LocalDamageDone, SourceTags);
				TargetCharacter->Execute_OnDamaged(TargetActor, LocalDamageDone, HitResult, SourceTags, SourceActor);
			}
		}
	}
}

void UEvilAttributeSet::OnRep_Health(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UEvilAttributeSet, Health, OldValue);
}

void UEvilAttributeSet::OnRep_MaxHealth(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UEvilAttributeSet, MaxHealth, OldValue);
}

void UEvilAttributeSet::OnRep_AttackPower(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UEvilAttributeSet, AttackPower, OldValue);
}

void UEvilAttributeSet::OnRep_DefensePower(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UEvilAttributeSet, DefensePower, OldValue);
}

void UEvilAttributeSet::OnRep_MoveSpeed(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UEvilAttributeSet, MoveSpeed, OldValue);
}

void UEvilAttributeSet::OnRep_FieldOfView(const FGameplayAttributeData& OldValue)
{
	GAMEPLAYATTRIBUTE_REPNOTIFY(UEvilAttributeSet, FieldOfView, OldValue);
}

