// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Abilities/GameplayAbility.h"
#include "EvilGameplayAbility.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilGameplayAbility : public UGameplayAbility
{
	GENERATED_BODY()
	
public:
	UEvilGameplayAbility();

	// TODO check the FGSGameplayEffectContainer usage in RPG template
};
