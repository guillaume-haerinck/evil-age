// MIT License

#pragma once

#include "CoreMinimal.h"
#include "AbilitySystemComponent.h"
#include "EvilGameplayAbility.h"
#include "EvilAbilitySystemComponent.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilAbilitySystemComponent : public UAbilitySystemComponent
{
	GENERATED_BODY()

public:
	UEvilAbilitySystemComponent();

	/** Returns a list of currently active ability instances that match the tags */
	UFUNCTION(BlueprintCallable)
	void GetActiveAbilitiesWithTags(const struct FGameplayTagContainer& GameplayTagContainer, TArray<UEvilGameplayAbility*>& ActiveAbilities);
};
