
#include "EvilBlueprintLibrary.h"
#include "Math/UnrealMathUtility.h"
#include "Pawns/EvilCharacterBase.h"
#include "Components/WidgetComponent.h"
#include "Kismet/KismetMathLibrary.h"
#include "Animation/AnimMontage.h"

void UEvilBlueprintLibrary::SortCharacteristics(TArray<FCharacterCharacteristics>& characteristics)
{
	characteristics.Sort([](const FCharacterCharacteristics& a, const FCharacterCharacteristics& b)
	{
		int aScore = 0;
		int bScore = 0;
		(a.Fitness.AttacksGiven > b.Fitness.AttacksGiven) ? aScore++ : bScore++;
		(a.Fitness.RessourceGathered > b.Fitness.RessourceGathered) ? aScore++ : bScore++;
		(a.Fitness.SecondsAlive > b.Fitness.SecondsAlive) ? aScore++ : bScore++;
		return aScore > bScore;
	});
}

void UEvilBlueprintLibrary::MutateGenome(FGenome& Genome)
{
	// As attribute, might also be clamped in the PostGameplayEffectExecute() method of EvilAttributeSet
	Genome.Health = FMath::Clamp(FMath::RandRange(Genome.Health - 10.f, Genome.Health + 10.f), 10.0f, 200.0f);
	Genome.AttackPower = FMath::Clamp(FMath::RandRange(Genome.AttackPower - 10.f, Genome.AttackPower + 10.f), 10.0f, 200.0f);
	Genome.DefensePower = FMath::Clamp(FMath::RandRange(Genome.DefensePower - 10.f, Genome.DefensePower + 10.f), 10.0f, 200.0f);
	Genome.MoveSpeed = FMath::Clamp(FMath::RandRange(Genome.MoveSpeed - 10.f, Genome.MoveSpeed + 10.f), 10.0f, 200.0f);
	Genome.FieldOfView = FMath::Clamp(FMath::RandRange(Genome.FieldOfView - 10.f, Genome.FieldOfView + 10.f), 10.0f, 200.0f);
}

int32 UEvilBlueprintLibrary::GetNumMontageSections(const UAnimMontage* montage)
{
	if (montage == nullptr)
		return 0;
	return montage->CompositeSections.Num();
}
