// Copyright Epic Games, Inc. All Rights Reserved.

#include "EvilAge.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, EvilAge, "EvilAge" );
