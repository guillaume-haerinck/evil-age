// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EvilItemBase.generated.h"


/**
 * Items such as weapons, armors and potions
 */
UCLASS()
class EVILAGE_API AEvilItemBase : public AActor
{
	GENERATED_BODY()
	
public:
	AEvilItemBase();

	/** Ability to grant if this item is slotted */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Abilities)
	TSubclassOf<class UEvilGameplayAbility> GrantedAbility;
};
