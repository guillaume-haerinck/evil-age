// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameplayTagContainer.h"
#include "DamageableInterface.generated.h"


/**
 * Only getter as damages are handled through ability system.
 * Allows the Ability system to process characters and buildings with the same code.
 */
UINTERFACE(Blueprintable)
class EVILAGE_API UDamageableInterface : public UInterface
{
	GENERATED_BODY()
};

class IDamageableInterface
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Health")
	float GetHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Health")
	float GetMaxHealth() const;

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void OnHoverStart();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void OnHoverEnd();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void OnSelectStart();

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "UI")
	void OnSelectEnd();


protected:
	/**
	 * Called when health is changed, either from healing or from being damaged
	 * For damage this is called in addition to OnDamaged/OnKilled
	 * 
	 * @note Called by UEvilAttributeSet
	 *
	 * @param DeltaValue Change in health value, positive for heal, negative for cost. If 0 the delta is unknown
	 * @param EventTags The gameplay tags of the event that changed mana
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "Health")
	void OnHealthChanged(float DeltaValue, const FGameplayTagContainer& EventTags);

	/**
	 * Called when character takes damage, which may have killed them
	 *
	 * @note Called by UEvilAttributeSet
	 * 
	 * @param DamageAmount Amount of damage that was done, not clamped based on current health
	 * @param HitInfo The hit info that generated this damage
	 * @param DamageTags The gameplay tags of the event that did the damage
	 * @param DamageCauser The actual actor that did the damage, might be a weapon or projectile
	 */
	UFUNCTION(BlueprintImplementableEvent, Category = "Health")
	void OnDamaged(float DamageAmount, const struct FHitResult& HitInfo, const FGameplayTagContainer& DamageTags, class AActor* DamageCauser);
};
