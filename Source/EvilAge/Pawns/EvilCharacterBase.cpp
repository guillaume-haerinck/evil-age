// MIT License

#include "Pawns/EvilCharacterBase.h"
#include "Components/WidgetComponent.h"
#include "GameMode/EvilGameModeBase.h"
#include "Abilities/EvilAttributeSet.h"
#include "Abilities/EvilAbilitySystemComponent.h"
#include "GameplayTagContainer.h"
#include "Subsystems/LevelActorReferencesSubsystem.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/EngineTypes.h"
#include "Components/SkeletalMeshComponent.h"
#include "Runtime/Engine/Classes/GameFramework/PlayerController.h"
#include "EvilBlueprintLibrary.h"

AEvilCharacterBase::AEvilCharacterBase() 
	: TeamId(0)
{
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Type::Disabled;
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;

	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UEvilAbilitySystemComponent>(TEXT("Ability System Component"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Create the attribute set, this replicates by default
	AttributeSet = CreateDefaultSubobject<UEvilAttributeSet>(TEXT("Attribute Set"));

	HealthWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Health Widget"));
	HealthWidget->SetupAttachment(RootComponent);
	HealthWidget->SetWidgetSpace(EWidgetSpace::Screen);
	HealthWidget->SetRelativeLocation(FVector(0, 0, 150));

	SelectionWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Selection Widget"));
	SelectionWidget->SetupAttachment(RootComponent);
}

void AEvilCharacterBase::PossessedBy(AController* NewController)
{
	Super::PossessedBy(NewController);
	AbilitySystemComponent->InitAbilityActorInfo(this, this);
	GiveStartupGameplayAbilities();
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();

	// Only apply genetic algorithm to enemies from the second wave
	if (GameMode->GetDayCount() == 0 || Type != CharacterType::ENEMY)
		ApplyDefaultAttributes();
	else
		ApplyGeneticAlgorithmAttributes();

	// Fill genome with base values
	Genome.AttackPower = GetAttackPower();
	Genome.DefensePower = GetDefensePower();
	Genome.FieldOfView = GetFieldOfView();
	Genome.Health = Execute_GetHealth(this);
	Genome.MoveSpeed = GetMoveSpeed();
}

UAbilitySystemComponent* AEvilCharacterBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

bool AEvilCharacterBase::ActivateAbilitiesWithTags(FGameplayTagContainer AbilityTags, bool bAllowRemoteActivation)
{
	if (IsValid(AbilitySystemComponent))
	{
		return AbilitySystemComponent->TryActivateAbilitiesByTag(AbilityTags, bAllowRemoteActivation);
	}

	return false;
}

void AEvilCharacterBase::GetActiveAbilitiesWithTags(FGameplayTagContainer AbilityTags, TArray<UEvilGameplayAbility*>& ActiveAbilities)
{
	if (IsValid(AbilitySystemComponent))
	{
		AbilitySystemComponent->GetActiveAbilitiesWithTags(AbilityTags, ActiveAbilities);
	}
}

void AEvilCharacterBase::BeginPlay()
{
	Super::BeginPlay();

	USkeletalMeshComponent* SkelMesh = Cast<USkeletalMeshComponent>(GetComponentByClass(USkeletalMeshComponent::StaticClass()));
	SkelMesh->OnBeginCursorOver.AddDynamic(this, &AEvilCharacterBase::OnBeginMouseOver);
	SkelMesh->OnEndCursorOver.AddDynamic(this, &AEvilCharacterBase::OnEndMouseOver);

	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->TimeStateChangedEvent.AddUObject(this, &AEvilCharacterBase::OnTimeStateChanged);

	// Add reference
	ULevelActorReferencesSubsystem* ReferenceSubsystem = GetWorld()->GetSubsystem<ULevelActorReferencesSubsystem>();
	if (ReferenceSubsystem)
		ReferenceSubsystem->AddSpawnedCharacterReference(this);
}

void AEvilCharacterBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	Fitness.SecondsAlive = GetGameTimeSinceCreation();
	if (Type == CharacterType::ENEMY)
	{
		AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
		GameMode->AddDeadEnemyCharacteristics(FCharacterCharacteristics(Genome, Fitness));
	}

	// Remove reference
	ULevelActorReferencesSubsystem* ReferenceSubsystem = GetWorld()->GetSubsystem<ULevelActorReferencesSubsystem>();
	if (ReferenceSubsystem)
		ReferenceSubsystem->RemoveCharacter(this);
}

void AEvilCharacterBase::OnBeginMouseOver(UPrimitiveComponent* TouchedComponent)
{
	//HealthWidget->SetVisibility(true);
}

void AEvilCharacterBase::OnEndMouseOver(UPrimitiveComponent* TouchedComponent)
{
	//HealthWidget->SetVisibility(false);
}

void AEvilCharacterBase::GiveStartupGameplayAbilities()
{
	if (HasAuthority() && IsValid(AbilitySystemComponent))
	{
		for (TSubclassOf<UEvilGameplayAbility>& StartupAbility : StartupGameplayAbilities)
		{
			FGameplayAbilitySpecHandle NewHandle = AbilitySystemComponent->GiveAbility(FGameplayAbilitySpec(StartupAbility, GetCharacterLevel(), INDEX_NONE, this));
			if (!NewHandle.IsValid())
				UE_LOG(LogTemp, Warning, TEXT("New ability not given to AActor %f"), *GetName());
		}
	}
}

void AEvilCharacterBase::ApplyDefaultAttributes()
{
	if (IsValid(AbilitySystemComponent) && DefaultAttributes)
	{
		FGameplayEffectContextHandle EffectContext = AbilitySystemComponent->MakeEffectContext();
		EffectContext.AddSourceObject(this);

		FGameplayEffectSpecHandle NewHandle = AbilitySystemComponent->MakeOutgoingSpec(DefaultAttributes, GetCharacterLevel(), EffectContext);
		if (NewHandle.IsValid())
		{
			FActiveGameplayEffectHandle ActiveGEHandle = AbilitySystemComponent->ApplyGameplayEffectSpecToSelf(*NewHandle.Data.Get());
		}
	}
}

void AEvilCharacterBase::ApplyGeneticAlgorithmAttributes()
{
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	FGenome NewGenome = GameMode->GetNewGenome();

	UGameplayEffect* GE_Genome = NewObject<UGameplayEffect>(GetTransientPackage(), FName(TEXT("GeneticAlgorithm")));
	GE_Genome->DurationPolicy = EGameplayEffectDurationType::Instant;

	int32 Idx = GE_Genome->Modifiers.Num();
	GE_Genome->Modifiers.SetNum(Idx + 5);

	FGameplayModifierInfo& Info1 = GE_Genome->Modifiers[Idx];
	Info1.ModifierMagnitude = FScalableFloat(NewGenome.Health);
	Info1.ModifierOp = EGameplayModOp::Override;
	Info1.Attribute = UEvilAttributeSet::GetHealthAttribute();

	FGameplayModifierInfo& Info2 = GE_Genome->Modifiers[Idx + 1];
	Info2.ModifierMagnitude = FScalableFloat(NewGenome.AttackPower);
	Info2.ModifierOp = EGameplayModOp::Override;
	Info2.Attribute = UEvilAttributeSet::GetAttackPowerAttribute();

	FGameplayModifierInfo& Info3 = GE_Genome->Modifiers[Idx + 2];
	Info3.ModifierMagnitude = FScalableFloat(NewGenome.DefensePower);
	Info3.ModifierOp = EGameplayModOp::Override;
	Info3.Attribute = UEvilAttributeSet::GetDefensePowerAttribute();

	FGameplayModifierInfo& Info4 = GE_Genome->Modifiers[Idx + 3];
	Info4.ModifierMagnitude = FScalableFloat(NewGenome.MoveSpeed);
	Info4.ModifierOp = EGameplayModOp::Override;
	Info4.Attribute = UEvilAttributeSet::GetMoveSpeedAttribute();

	FGameplayModifierInfo& Info5 = GE_Genome->Modifiers[Idx + 4];
	Info5.ModifierMagnitude = FScalableFloat(NewGenome.FieldOfView);
	Info5.ModifierOp = EGameplayModOp::Override;
	Info5.Attribute = UEvilAttributeSet::GetFieldOfViewAttribute();

	AbilitySystemComponent->ApplyGameplayEffectToSelf(GE_Genome, GetCharacterLevel(), AbilitySystemComponent->MakeEffectContext());
}

void AEvilCharacterBase::Kill()
{
	AttributeSet->SetHealth(-1.0f);
}

bool AEvilCharacterBase::IsAlive() const
{
	return Execute_GetHealth(this) > 0.0f;
}

void AEvilCharacterBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

int32 AEvilCharacterBase::GetCharacterLevel() const
{
	return 1;
}

CharacterType AEvilCharacterBase::GetCharacterType() const
{
	return Type;
}

float AEvilCharacterBase::GetMoveSpeed() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetMoveSpeed();
	}
	return 0.0f;
}

float AEvilCharacterBase::GetFieldOfView() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetFieldOfView();
	}
	return 0.0f;
}


float AEvilCharacterBase::GetHealth_Implementation() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetHealth();
	}
	return 0.f;
}

float AEvilCharacterBase::GetMaxHealth_Implementation() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetMaxHealth();
	}
	return 0.f;
}


float AEvilCharacterBase::GetAttackPower() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetAttackPower();
	}
	return 0.f;
}

float AEvilCharacterBase::GetDefensePower() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetDefensePower();
	}
	return 0.f;
}

FGenericTeamId AEvilCharacterBase::GetGenericTeamId() const
{
	return TeamId;
}

ETeamAttitude::Type AEvilCharacterBase::GetTeamAttitudeTowards(const AActor& Other) const
{
	const IGenericTeamAgentInterface* OtherTeamAgent = Cast<const IGenericTeamAgentInterface>(&Other);
	if (OtherTeamAgent->GetGenericTeamId() == GetGenericTeamId())
		return ETeamAttitude::Friendly;
	else
		return ETeamAttitude::Hostile;
}
