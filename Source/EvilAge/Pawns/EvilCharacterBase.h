// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "EvilTypes.h"
#include "Abilities/EvilGameplayAbility.h"
#include "Interfaces/DamageableInterface.h"
#include "AbilitySystemInterface.h"
#include "GenericTeamAgentInterface.h"
#include "EvilCharacterBase.generated.h"

/**
 * Base class for enemies, villagers and knights. They are using the ability system.
 */
UCLASS()
class EVILAGE_API AEvilCharacterBase : public ACharacter, public IDamageableInterface, public IAbilitySystemInterface, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

public:
	// Constructor and overrides
	AEvilCharacterBase();
	virtual void PossessedBy(AController* NewController) override;
	virtual void Tick(float DeltaTime) override;

	void Kill();

	UFUNCTION(BlueprintCallable)
	virtual bool IsAlive() const;

	// Implement IAbilitySystemInterface
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	// Implement IGenericTeamInterface 
	UFUNCTION(BlueprintCallable, Category = "AI")
	virtual FGenericTeamId GetGenericTeamId() const override;

	virtual ETeamAttitude::Type GetTeamAttitudeTowards(const AActor& Other) const override;

	// Returns the character level that is passed to the ability system
	UFUNCTION(BlueprintCallable)
	virtual int32 GetCharacterLevel() const;

	UFUNCTION(BlueprintCallable)
	CharacterType GetCharacterType() const;

	// ----- Shortcut methods ------

	UFUNCTION(BlueprintCallable)
	float GetFieldOfView() const;

	UFUNCTION(BlueprintCallable)
	float GetMoveSpeed() const;

	UFUNCTION(BlueprintCallable)
	float GetAttackPower() const;

	UFUNCTION(BlueprintCallable)
	float GetDefensePower() const;

	// Implement IDamageableInterface
	float GetHealth_Implementation() const override;
	float GetMaxHealth_Implementation() const override;

	/**
	 * Attempts to activate all abilities that match the specified tags
	 * Returns true if it thinks it activated, but it may return false positives due to failure later in activation.
	 * If bAllowRemoteActivation is true, it will remotely activate local/server abilities, if false it will only try to locally activate the ability
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	bool ActivateAbilitiesWithTags(struct FGameplayTagContainer AbilityTags, bool bAllowRemoteActivation = true);

	/**
	 * Returns a list of active abilities matching the specified tags. This only returns if the ability is currently running
	 */
	UFUNCTION(BlueprintCallable, Category = "Abilities")
	void GetActiveAbilitiesWithTags(struct FGameplayTagContainer AbilityTags, TArray<UEvilGameplayAbility*>& ActiveAbilities);

	// ------ End shortcut methods ------

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;
	virtual void GiveStartupGameplayAbilities();
	virtual void ApplyDefaultAttributes();
	virtual void ApplyGeneticAlgorithmAttributes();

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	FGenericTeamId TeamId;

	UFUNCTION()
	void OnBeginMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION()
	void OnEndMouseOver(UPrimitiveComponent* TouchedComponent);

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "UI")
	class UWidgetComponent* HealthWidget;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "UI")
	class UWidgetComponent* SelectionWidget;

	// Abilities to grant to this character on creation. These will be activated by tag or event and are not bound to specific inputs
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Abilities")
	TArray<TSubclassOf<UEvilGameplayAbility>> StartupGameplayAbilities;

	// Default attributes for a character for initializing on spawn/respawn.
	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = "Abilities")
	TSubclassOf<class UGameplayEffect> DefaultAttributes;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities")
	class UEvilAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities")
	class UEvilAttributeSet* AttributeSet;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities")
	CharacterType Type;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Abilities")
	FFitness Fitness;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Abilities")
	FGenome Genome;
};
