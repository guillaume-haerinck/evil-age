// MIT License

#include "Pawns/EvilGodBase.h"
#include "Components/SceneComponent.h"
#include "Camera/CameraComponent.h"
#include "Engine/CollisionProfile.h"
#include "Kismet/KismetMathLibrary.h"

// Sets default values
AEvilGodBase::AEvilGodBase()
{
	PrimaryActorTick.bCanEverTick = true;
	AutoPossessPlayer = EAutoReceiveInput::Player0;
	AutoPossessAI = EAutoPossessAI::Disabled;

	SceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = SceneComponent;

	// Initialize the camera
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("RTS Camera"));
	CameraComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::KeepRelativeTransform);
	CameraComponent->bUsePawnControlRotation = false;

	UpdateCameraLocationRotation();
}

void AEvilGodBase::OnZoomIn()
{
	CameraRadius = FMath::Clamp(CameraRadius - ZoomSpeed, RadiusMin, RadiusMax);
	UpdateCameraLocationRotation();
}

void AEvilGodBase::OnZoomOut()
{
	CameraRadius = FMath::Clamp(CameraRadius + ZoomSpeed, RadiusMin, RadiusMax);
	UpdateCameraLocationRotation();
}

void AEvilGodBase::OnLookUp(float AxisValue)
{
	if (!bIsCameraRotating)
		return;

	CameraPitch = FMath::Clamp(CameraPitch + RotateSpeed * AxisValue, 5.f, 90.f);
	UpdateCameraLocationRotation();
}

void AEvilGodBase::OnTurn(float AxisValue)
{
	if (!bIsCameraRotating)
		return;

	CameraYaw = CameraYaw + RotateSpeed * AxisValue;
	UpdateCameraLocationRotation();
}

void AEvilGodBase::OnMoveForward(float AxisValue)
{
	if (FMath::Abs(AxisValue) < 0.1f)
		return;

	const float speed = AxisValue * MovementSpeed;
	const FVector forward = UKismetMathLibrary::GetForwardVector(FRotator(0, CameraComponent->GetComponentRotation().Yaw, 0));
	const FVector newLocation = GetActorLocation() + forward * speed;
	SetActorLocation(newLocation);
}

void AEvilGodBase::OnMoveRight(float AxisValue)
{
	if (FMath::Abs(AxisValue) < 0.1f)
		return;

	const float speed = AxisValue * MovementSpeed;
	const FVector right = UKismetMathLibrary::GetRightVector(FRotator(0, CameraComponent->GetComponentRotation().Yaw, 0));
	const FVector newLocation = GetActorLocation() + right * speed;
	SetActorLocation(newLocation);
}

void AEvilGodBase::UpdateCameraLocationRotation()
{
	FVector newLocation = PointOnASphere(CameraRadius, CameraYaw, CameraPitch);
	CameraComponent->SetRelativeLocation(newLocation);
	FRotator newRotation = UKismetMathLibrary::FindLookAtRotation(CameraComponent->GetRelativeLocation(), FVector(0, 0, 0));
	CameraComponent->SetRelativeRotation(newRotation);
}

FVector AEvilGodBase::PointOnASphere(float Radius, float Yaw, float Pitch)
{
	const float yaw = FMath::DegreesToRadians(Yaw);
	const float pitch = FMath::DegreesToRadians(Pitch);
	FVector point;
	point.X = FMath::Cos(yaw) * FMath::Sin(pitch) * Radius;
	point.Y = FMath::Sin(yaw) * FMath::Sin(pitch) * Radius;
	point.Z = FMath::Cos(pitch) * Radius;
	return point;
}

