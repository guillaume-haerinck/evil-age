// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "EvilGodBase.generated.h"

UCLASS()
class EVILAGE_API AEvilGodBase : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AEvilGodBase();

	UFUNCTION(BlueprintCallable, Category = Camera)
	void OnZoomIn();

	UFUNCTION(BlueprintCallable, Category = Camera)
	void OnZoomOut();

	UFUNCTION(BlueprintCallable, Category = Camera)
	void OnLookUp(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = Camera)
	void OnTurn(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = Camera)
	void OnMoveForward(float AxisValue);

	UFUNCTION(BlueprintCallable, Category = Camera)
	void OnMoveRight(float AxisValue);

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class UCameraComponent* CameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera)
	class USceneComponent* SceneComponent;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float CameraRadius = 1000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float CameraYaw = 0.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float CameraPitch = 45.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float RotateSpeed = 2.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float ZoomSpeed = 100.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float RadiusMin = 128.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float RadiusMax = 3000.0f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Camera)
	float MovementSpeed = 10.0f;

	UPROPERTY(BlueprintReadWrite, Category = Camera)
	bool bIsCameraRotating = false;

	UFUNCTION(BlueprintCallable, Category = Camera)
	void UpdateCameraLocationRotation();

private:
	/**
	 * Returns a location from the given sphere angle
	 * 
	 * @param Radius - Sphere radius in Unreal unit
	 * @param Yaw - Angle around Z axis in degrees
	 * @param Pitch - Angle for height in degrees
	 */
	FVector PointOnASphere(float Radius, float Yaw, float Pitch);
};
