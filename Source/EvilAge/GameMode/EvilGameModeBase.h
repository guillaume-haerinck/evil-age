// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "EvilTypes.h"
#include "Containers/Map.h"
#include "EvilGameModeBase.generated.h"

/**
 * Default game mode with day/night cycle and enemy waves
 */
UCLASS()
class EVILAGE_API AEvilGameModeBase : public AGameModeBase
{
	GENERATED_BODY()

public:
	AEvilGameModeBase(const FObjectInitializer& ObjectInitializer);

protected:
	virtual void BeginPlay() override;

	virtual void Tick(float DeltaSeconds) override;

	void KillAllEnemies();

	void ToggleLevelLights(bool enabled);

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int NumberOfMonstersToSpawnPerAreas;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float TimeStep;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Money;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	int Faith;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

	UFUNCTION(BlueprintImplementableEvent)
	void OnMoneyChanged();

	UFUNCTION(BlueprintImplementableEvent)
	void OnFaithChanged();

public:
	DECLARE_EVENT_OneParam(AEvilGameModeBase, FTimeStateChanged, TimeState);
	DECLARE_EVENT_OneParam(AEvilGameModeBase, FSpawnEnemyWave, int);

	FTimeStateChanged TimeStateChangedEvent;
	FSpawnEnemyWave SpawnEnemyWaveEvent;

	UFUNCTION(BlueprintCallable, meta = (ToolTip="Between 0 and 1. Night is between 0 and 0.5, Day between 0.5 and 1"))
	float GetTimeStateContinuous() { return TimeStateContinuous; }

	UFUNCTION(BlueprintCallable)
	int GetDayCount() { return DayCount; }

	UFUNCTION(BlueprintCallable)
	void ChangeMoney(int DeltaValue);

	UFUNCTION(BlueprintCallable)
	int GetMoney() { return Money; }

	UFUNCTION(BlueprintCallable)
	void ChangeFaith(int DeltaValue);

	UFUNCTION(BlueprintCallable)
	int GetFaith() { return Faith; }

	void AddDeadEnemyCharacteristics(FCharacterCharacteristics Characteristics);
	FGenome GetNewGenome();

private:
	void UpdateTimeState();

	float TimeStateContinuous;
	int DayCount;
	TimeState CurrentTimeState;
	TArray<class AActor*> LevelLights;
	TArray<FCharacterCharacteristics> LastWaveEnemyCharacteristics;
	bool bEnemyCharacteristicsSorted;
};
