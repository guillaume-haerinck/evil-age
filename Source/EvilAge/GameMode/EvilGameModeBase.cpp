// Copyright Epic Games, Inc. All Rights Reserved.

#include "EvilGameModeBase.h"

#include "Kismet/GameplayStatics.h"
#include "Engine/PointLight.h"
#include "Components/PointLightComponent.h"
#include "Subsystems/LevelActorReferencesSubsystem.h"
#include "EvilPlayerControllerBase.h"
#include "EvilGameStateBase.h"
#include "Pawns/EvilGodBase.h"
#include "Pawns/EvilCharacterBase.h"
#include "UI/EvilWidgetBase.h"
#include "EvilBlueprintLibrary.h"

AEvilGameModeBase::AEvilGameModeBase(const FObjectInitializer& ObjectInitializer)
	: Super(ObjectInitializer), NumberOfMonstersToSpawnPerAreas(1), TimeStep(0.0001f), 
	  TimeStateContinuous(0.5f), CurrentTimeState(TimeState::MORNING), DayCount(0),
	  bEnemyCharacteristicsSorted(false), Money(100), Faith(100)
{
	PrimaryActorTick.bCanEverTick = true;
	PlayerControllerClass = AEvilPlayerControllerBase::StaticClass();
	DefaultPawnClass = AEvilGodBase::StaticClass();
	GameStateClass = AEvilGameStateBase::StaticClass();
	HUDClass = UEvilWidgetBase::StaticClass();
}

void AEvilGameModeBase::BeginPlay()
{
	Super::BeginPlay();
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APointLight::StaticClass(), LevelLights);
	TimeStateChangedEvent.AddUObject(this, &AEvilGameModeBase::OnTimeStateChanged);
	TimeStateChangedEvent.Broadcast(CurrentTimeState);
}

void AEvilGameModeBase::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);
	UpdateTimeState();
}

void AEvilGameModeBase::KillAllEnemies()
{
	ULevelActorReferencesSubsystem* ReferenceSubsystem = GetWorld()->GetSubsystem<ULevelActorReferencesSubsystem>();
	if (!ReferenceSubsystem)
		return;

	for (auto& Enemy : ReferenceSubsystem->GetSpawnedEnemiesReferences())
		Enemy->Kill();
}

void AEvilGameModeBase::ToggleLevelLights(bool enabled)
{
	for (AActor* actor : LevelLights)
	{
		APointLight* light = Cast<APointLight>(actor);
		light->PointLightComponent->SetVisibility(enabled);
	}
}

void AEvilGameModeBase::ChangeMoney(int DeltaValue)
{
	Money += DeltaValue;
	Money = FMath::Clamp(Money, 0, 1000000);
	OnMoneyChanged();
}

void AEvilGameModeBase::ChangeFaith(int DeltaValue)
{
	Faith += DeltaValue;
	Faith = FMath::Clamp(Faith, 0, 1000000);
	OnFaithChanged();
}

void AEvilGameModeBase::AddDeadEnemyCharacteristics(FCharacterCharacteristics Characteristics)
{
	LastWaveEnemyCharacteristics.Add(Characteristics);
	bEnemyCharacteristicsSorted = false;
}

FGenome AEvilGameModeBase::GetNewGenome()
{
	if (!bEnemyCharacteristicsSorted)
	{
		UEvilBlueprintLibrary::SortCharacteristics(LastWaveEnemyCharacteristics);
		bEnemyCharacteristicsSorted = true;
	}

	if (LastWaveEnemyCharacteristics.Num() == 0)
	{
		UE_LOG(LogTemp, Warning, TEXT("[EvilGameModeBase] Tried to get new genome but it is first wave and genome data is empty"));
		return FGenome();
	}
	else
	{
		FGenome NewGenome = LastWaveEnemyCharacteristics[0].Genome;
		UEvilBlueprintLibrary::MutateGenome(NewGenome);
		return NewGenome;
	}
}

void AEvilGameModeBase::UpdateTimeState()
{
	TimeStateContinuous += TimeStep;
	switch (CurrentTimeState)
	{
	case TimeState::EVENING:
		if (TimeStateContinuous >= 0.17f)
		{
			CurrentTimeState = TimeState::MIDNIGHT;
			TimeStateChangedEvent.Broadcast(CurrentTimeState);
			SpawnEnemyWaveEvent.Broadcast(NumberOfMonstersToSpawnPerAreas);
			ToggleLevelLights(true);
		}
		break;

	case TimeState::MIDNIGHT:
		if (TimeStateContinuous >= 0.34f)
		{
			CurrentTimeState = TimeState::BEFORE_MORNING;
			TimeStateChangedEvent.Broadcast(CurrentTimeState);
		}
		break;

	case TimeState::BEFORE_MORNING:
		if (TimeStateContinuous >= 0.5f)
		{
			CurrentTimeState = TimeState::MORNING;
			TimeStateChangedEvent.Broadcast(CurrentTimeState);
			DayCount++;
			KillAllEnemies();
			LastWaveEnemyCharacteristics.Empty();
			ToggleLevelLights(false);
		}
		break;

	case TimeState::MORNING:
		if (TimeStateContinuous >= 0.68f)
		{
			CurrentTimeState = TimeState::NOON;
			TimeStateChangedEvent.Broadcast(CurrentTimeState);
		}
		break;

	case TimeState::NOON:
		if (TimeStateContinuous >= 0.85f)
		{
			CurrentTimeState = TimeState::AFTERNOON;
			TimeStateChangedEvent.Broadcast(CurrentTimeState);
		}
		break;

	case TimeState::AFTERNOON:
		if (TimeStateContinuous >= 1.f)
		{
			CurrentTimeState = TimeState::EVENING;
			TimeStateChangedEvent.Broadcast(CurrentTimeState);
			TimeStateContinuous = 0.f;
		}
		break;

	default: break;
	}
}
