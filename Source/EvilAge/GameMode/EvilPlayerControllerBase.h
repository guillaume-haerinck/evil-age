// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "EvilPlayerControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API AEvilPlayerControllerBase : public APlayerController
{
	GENERATED_BODY()
	
public:
	AEvilPlayerControllerBase(const FObjectInitializer& ObjectInitializer);
};
