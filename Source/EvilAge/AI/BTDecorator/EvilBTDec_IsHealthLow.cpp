// MIT License


#include "AI/BTDecorator/EvilBTDec_IsHealthLow.h"
#include "GameFramework/Pawn.h"
#include "AIController.h"
#include "Interfaces/DamageableInterface.h"

bool UEvilBTDec_IsHealthLow::CalculateRawConditionValue(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) const
{
	Super::CalculateRawConditionValue(OwnerComp, NodeMemory);

	const IDamageableInterface* Damageable = Cast<IDamageableInterface>(OwnerComp.GetAIOwner()->GetPawn());
	if (ensure(Damageable))
	{
		const float Current = Damageable->Execute_GetHealth(OwnerComp.GetAIOwner()->GetPawn());
		const float Max = Damageable->Execute_GetMaxHealth(OwnerComp.GetAIOwner()->GetPawn());
		if (Current / Max <= MinHealthPercentage)
			return true;
		else
			return false;
	}

	return false;
}
