// MIT License


#include "AI/EvilAIControllerBase.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Perception/AIPerceptionComponent.h"
#include "Perception/AISenseConfig_Sight.h"
#include "GameMode/EvilGameModeBase.h"

AEvilAIControllerBase::AEvilAIControllerBase()
{
	AIPerceptionComp = CreateDefaultSubobject<UAIPerceptionComponent>(TEXT("AI Perception"));
	AIConfigSight = CreateDefaultSubobject<UAISenseConfig_Sight>(TEXT("AI Sight"));

	// Config sight defaults
	AIConfigSight->DetectionByAffiliation.bDetectEnemies = true;
	AIConfigSight->DetectionByAffiliation.bDetectFriendlies = false;
	AIConfigSight->DetectionByAffiliation.bDetectNeutrals = false;
	AIConfigSight->SightRadius = 1000.0f;
	AIConfigSight->LoseSightRadius = 2000.0f;
	AIConfigSight->PeripheralVisionAngleDegrees = 90.0f;
	
	AIPerceptionComp->ConfigureSense(*AIConfigSight);
	AIPerceptionComp->SetDominantSense(UAISenseConfig_Sight::StaticClass());
}

void AEvilAIControllerBase::BeginPlay()
{
	Super::BeginPlay();

	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->TimeStateChangedEvent.AddUObject(this, &AEvilAIControllerBase::OnTimeStateChanged);

	if (ensureMsgf(BehaviorTree, TEXT("Behavior Tree is nullptr! Please assign BehaviorTree in your AI Controller.")))
	{
		RunBehaviorTree(BehaviorTree);
	}

	/*
	APawn* MyPawn = UGameplayStatics::GetPlayerPawn(this, 0);
	if (MyPawn)
	{
		GetBlackboardComponent()->SetValueAsVector("MoveToLocation", MyPawn->GetActorLocation());
		GetBlackboardComponent()->SetValueAsObject("TargetActor", MyPawn);
	}
	*/
}
