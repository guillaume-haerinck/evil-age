// MIT License

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "EvilBTTask_UseAbility.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilBTTask_UseAbility : public UBTTaskNode
{
	GENERATED_BODY()
	
public:
	UEvilBTTask_UseAbility();

protected:
	UPROPERTY(EditAnywhere, Category = "AI")
	struct FGameplayTagContainer GameplayAbilityTags;

	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	UFUNCTION()
	void OnAbilityEnded(UGameplayAbility* Ability);

	UBehaviorTreeComponent* OwnerCompCache;
};
