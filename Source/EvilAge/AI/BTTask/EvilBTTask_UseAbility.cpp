// MIT License


#include "EvilBTTask_UseAbility.h"
#include "GameFramework/Pawn.h"
#include "AIController.h"
#include "GameplayTagContainer.h"
#include "Pawns/EvilCharacterBase.h"

UEvilBTTask_UseAbility::UEvilBTTask_UseAbility()
{
	bCreateNodeInstance = true;
}

EBTNodeResult::Type UEvilBTTask_UseAbility::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	Super::ExecuteTask(OwnerComp, NodeMemory);

	AEvilCharacterBase* Character = Cast<AEvilCharacterBase>(OwnerComp.GetAIOwner()->GetPawn());
	if (ensure(Character))
	{
		Character->ActivateAbilitiesWithTags(GameplayAbilityTags);
		TArray<UEvilGameplayAbility*> ActiveAbilities;
		Character->GetActiveAbilitiesWithTags(GameplayAbilityTags, ActiveAbilities);

		if (ActiveAbilities.Num() == 0)
			return EBTNodeResult::Failed;

		UEvilGameplayAbility* Ability = Cast<UEvilGameplayAbility>(ActiveAbilities[0]);

		// Node only done when Ability is over
		Ability->OnGameplayAbilityEnded.AddUObject(this, &UEvilBTTask_UseAbility::OnAbilityEnded);
		OwnerCompCache = &OwnerComp;
		return EBTNodeResult::InProgress;
	}

	return EBTNodeResult::Failed;
}

void UEvilBTTask_UseAbility::OnAbilityEnded(UGameplayAbility* Ability)
{
	FinishLatentTask(*OwnerCompCache, EBTNodeResult::Succeeded);
}
