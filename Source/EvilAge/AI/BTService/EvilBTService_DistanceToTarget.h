// MIT License

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "EvilBTService_DistanceToTarget.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilBTService_DistanceToTarget : public UBTService
{
	GENERATED_BODY()
	
public:
	UEvilBTService_DistanceToTarget();

protected:
	UPROPERTY(EditAnywhere, Category = "AI")
	FBlackboardKeySelector DistanceToTargetKey;

	UPROPERTY(EditAnywhere, Category = "AI")
	FBlackboardKeySelector TargetToFollowKey;

	UPROPERTY(EditAnywhere, Category = "AI")
	float DistToTarget;

	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
