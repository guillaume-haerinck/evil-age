// MIT License


#include "AI/BTService/EvilBTService_DistanceToTarget.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "GameFramework/Pawn.h"
#include "AIController.h"
#include "Math/Vector.h"

UEvilBTService_DistanceToTarget::UEvilBTService_DistanceToTarget()
{

}

void UEvilBTService_DistanceToTarget::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	Super::TickNode(OwnerComp, NodeMemory, DeltaSeconds);

	const APawn* AIPawn = OwnerComp.GetAIOwner()->GetPawn();
	if (ensure(AIPawn))
	{
		UBlackboardComponent* BlackBoardComp = OwnerComp.GetBlackboardComponent();
		const AActor* TargetToFollow = Cast<AActor>(BlackBoardComp->GetValueAsObject(TargetToFollowKey.SelectedKeyName));
		if (ensure(TargetToFollow))
		{
			const float distance = FVector::Dist(TargetToFollow->GetActorLocation(), AIPawn->GetActorLocation());
			BlackBoardComp->SetValueAsFloat(DistanceToTargetKey.SelectedKeyName, distance);
		}
	}
}
