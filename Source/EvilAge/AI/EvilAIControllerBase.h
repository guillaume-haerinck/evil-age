// MIT License

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "EvilTypes.h"
#include "EvilAIControllerBase.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API AEvilAIControllerBase : public AAIController
{
	GENERATED_BODY()

public:
	AEvilAIControllerBase();

protected:
	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	UBehaviorTree* BehaviorTree;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	class UAIPerceptionComponent* AIPerceptionComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "AI")
	class UAISenseConfig_Sight* AIConfigSight;

	virtual void BeginPlay() override;
};
