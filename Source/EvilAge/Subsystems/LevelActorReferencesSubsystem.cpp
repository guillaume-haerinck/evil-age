// MIT License


#include "Subsystems/LevelActorReferencesSubsystem.h"
#include "GameFramework/Actor.h"
#include "Pawns/EvilCharacterBase.h"
#include "Building/EvilBuildingBase.h"
#include "EvilTypes.h"

void ULevelActorReferencesSubsystem::AddSpawnedCharacterReference(AEvilCharacterBase* Character)
{
	switch (Character->GetCharacterType())
	{
	case CharacterType::ENEMY: SpawnedEnemies.Add(Character); break;
	case CharacterType::KNIGHT: SpawnedKnights.Add(Character); break;
	case CharacterType::VILLAGER: SpawnedVillagers.Add(Character); break;
	case CharacterType::UNKNOWN:
	default:
		UE_LOG(LogTemp, Warning, TEXT("[LevelActorReferencesSubsystem] Tried to add character but character type is not set, %s"), *Character->GetName());
		break;
	}
}

bool ULevelActorReferencesSubsystem::RemoveCharacter(AEvilCharacterBase* Character)
{
	switch (Character->GetCharacterType())
	{
	case CharacterType::ENEMY: SpawnedEnemies.Remove(Character); return true;
	case CharacterType::KNIGHT: SpawnedKnights.Remove(Character); return true;
	case CharacterType::VILLAGER: SpawnedVillagers.Remove(Character); return true;
	case CharacterType::UNKNOWN:
	default:
		UE_LOG(LogTemp, Warning, TEXT("[LevelActorReferencesSubsystem] Tried to remove character but character type is not set, %s"), *Character->GetName());
		return false;
	}
}

const TArray<AEvilCharacterBase*>& ULevelActorReferencesSubsystem::GetSpawnedEnemiesReferences()
{
	return SpawnedEnemies;
}

const TArray<AEvilCharacterBase*>& ULevelActorReferencesSubsystem::GetSpawnedVillagersReferences()
{
	return SpawnedVillagers;
}

const TArray<AEvilCharacterBase*>& ULevelActorReferencesSubsystem::GetSpawnedKnightsReferences()
{
	return SpawnedKnights;
}

void ULevelActorReferencesSubsystem::AddSpawnedBuildingReference(AEvilBuildingBase* Building)
{
	switch (Building->GetBuildingType())
	{
	case BuildingType::HOUSE: SpawnedHouses.Add(Building); break;
	case BuildingType::RESSOURCE: SpawnedRessources.Add(Building); break;
	case BuildingType::UNKNOWN:
	default:
		UE_LOG(LogTemp, Warning, TEXT("[LevelActorReferencesSubsystem] Tried to add building but building type is not set, %s"), *Building->GetName());
		break;
	}
}

bool ULevelActorReferencesSubsystem::RemoveBuilding(AEvilBuildingBase* Building)
{
	switch (Building->GetBuildingType())
	{
	case BuildingType::HOUSE: SpawnedHouses.Remove(Building); return true;
	case BuildingType::RESSOURCE: SpawnedRessources.Remove(Building); return true;
	case BuildingType::UNKNOWN:
	default:
		UE_LOG(LogTemp, Warning, TEXT("[LevelActorReferencesSubsystem] Tried to remove building but building type is not set, %s"), *Building->GetName());
		return false;
	}
}

const TArray<AEvilBuildingBase*>& ULevelActorReferencesSubsystem::GetSpawnedHousesReferences()
{
	return SpawnedHouses;
}

const TArray<AEvilBuildingBase*>& ULevelActorReferencesSubsystem::GetSpawnedRessourcesReferences()
{
	return SpawnedRessources;
}