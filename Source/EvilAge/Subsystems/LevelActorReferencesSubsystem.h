// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Subsystems/WorldSubsystem.h"
#include "LevelActorReferencesSubsystem.generated.h"

/**
 * 
 */
UCLASS()
class EVILAGE_API ULevelActorReferencesSubsystem : public UWorldSubsystem
{
	GENERATED_BODY()

public:

	// ---- Characters ----

	void AddSpawnedCharacterReference(class AEvilCharacterBase* Character);
	bool RemoveCharacter(class AEvilCharacterBase* Character);

	UFUNCTION(BlueprintCallable)
	const TArray<class AEvilCharacterBase*>& GetSpawnedEnemiesReferences();

	UFUNCTION(BlueprintCallable)
	const TArray<class AEvilCharacterBase*>& GetSpawnedVillagersReferences();

	UFUNCTION(BlueprintCallable)
	const TArray<class AEvilCharacterBase*>& GetSpawnedKnightsReferences();

	// ---- Buildings ----

	void AddSpawnedBuildingReference(class AEvilBuildingBase* Building);
	bool RemoveBuilding(class AEvilBuildingBase* Building);

	UFUNCTION(BlueprintCallable)
	const TArray<class AEvilBuildingBase*>& GetSpawnedHousesReferences();

	UFUNCTION(BlueprintCallable)
	const TArray<class AEvilBuildingBase*>& GetSpawnedRessourcesReferences();

protected:
	UPROPERTY()
	TArray<class AEvilCharacterBase*> SpawnedEnemies;

	UPROPERTY()
	TArray<class AEvilCharacterBase*> SpawnedVillagers;

	UPROPERTY()
	TArray<class AEvilCharacterBase*> SpawnedKnights;

	UPROPERTY()
	TArray<class AEvilBuildingBase*> SpawnedHouses;

	UPROPERTY()
	TArray<class AEvilBuildingBase*> SpawnedRessources;
};
