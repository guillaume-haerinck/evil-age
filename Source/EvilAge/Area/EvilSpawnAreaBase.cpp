// MIT License


#include "Area/EvilSpawnAreaBase.h"
#include "Components/BoxComponent.h"

// Sets default values
AEvilSpawnAreaBase::AEvilSpawnAreaBase()
{
	BoxCollider = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxCollider"));
	BoxCollider->SetWorldScale3D(FVector(2.f, 2.f, 2.f));
}

// Called when the game starts or when spawned
void AEvilSpawnAreaBase::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AEvilSpawnAreaBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

FVector AEvilSpawnAreaBase::GetAvailablePosOnArea()
{
	return GetActorLocation();
}

