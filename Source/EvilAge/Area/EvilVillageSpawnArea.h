// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Area/EvilSpawnAreaBase.h"
#include "EvilVillageSpawnArea.generated.h"

/**
 * Handle procedural village, building construction and population spawn
 */
UCLASS()
class EVILAGE_API AEvilVillageSpawnArea : public AEvilSpawnAreaBase
{
	GENERATED_BODY()
	
};
