// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EvilSpawnAreaBase.generated.h"

/**
 * Base class for every spawn areas
 */
UCLASS()
class EVILAGE_API AEvilSpawnAreaBase : public AActor
{
	GENERATED_BODY()
	
public:
	AEvilSpawnAreaBase();

protected:
	virtual void BeginPlay() override;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable)
	FVector GetAvailablePosOnArea();

protected:
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UBoxComponent* BoxCollider;
};
