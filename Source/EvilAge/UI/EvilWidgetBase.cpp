// MIT License


#include "UI/EvilWidgetBase.h"
#include "GameMode/EvilGameModeBase.h"

UEvilWidgetBase::UEvilWidgetBase(const FObjectInitializer& ObjectInitializer) : UUserWidget(ObjectInitializer)
{
	bIsFocusable = true;
}

// Called when the game starts or when spawned
void UEvilWidgetBase::NativeOnInitialized()
{
	Super::NativeOnInitialized();

	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->TimeStateChangedEvent.AddUObject(this, &UEvilWidgetBase::OnTimeStateChanged);
}
