// MIT License

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EvilTypes.h"
#include "EvilWidgetBase.generated.h"

// TODO check if there is a way to get owner here automatically

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilWidgetBase : public UUserWidget
{
	GENERATED_BODY()

public:
	UEvilWidgetBase(const FObjectInitializer& ObjectInitializer);
	
protected:

	virtual void NativeOnInitialized() override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);
};
