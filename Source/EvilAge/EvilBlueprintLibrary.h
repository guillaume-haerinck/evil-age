// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "EvilTypes.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "EvilBlueprintLibrary.generated.h"

class AEvilCharacterBase; // Forward declaration

/**
 * 
 */
UCLASS()
class EVILAGE_API UEvilBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
	
public:
	// ---- Genetic algorithm ----

	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	static void SortCharacteristics(TArray<FCharacterCharacteristics>& characteristics);

	UFUNCTION(BlueprintCallable, Category = "Genetic Algorithm")
	static void MutateGenome(FGenome& Genome);

	// ---- Utils BP ----

	UFUNCTION(BlueprintCallable, Category = "Animation")
	static int32 GetNumMontageSections(const class UAnimMontage* montage);
};
