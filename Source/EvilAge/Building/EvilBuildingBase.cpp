// MIT License

#include "Building/EvilBuildingBase.h"
#include "GameMode/EvilGameModeBase.h"
#include "Components/StaticMeshComponent.h"
#include "Components/WidgetComponent.h"
#include "Subsystems/LevelActorReferencesSubsystem.h"
#include "Abilities/EvilAbilitySystemComponent.h"
#include "Abilities/EvilAttributeSet.h"
#include "Abilities/EvilGameplayAbility.h"

// Sets default values
AEvilBuildingBase::AEvilBuildingBase() : TeamId(0), Type(BuildingType::UNKNOWN)
{
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Mesh"));
	Mesh->SetWorldScale3D(FVector(0.5f));
	RootComponent = Mesh;

	// Create ability system component, and set it to be explicitly replicated
	AbilitySystemComponent = CreateDefaultSubobject<UEvilAbilitySystemComponent>(TEXT("Ability System Component"));
	AbilitySystemComponent->SetIsReplicated(true);

	// Create the attribute set, this replicates by default
	AttributeSet = CreateDefaultSubobject<UEvilAttributeSet>(TEXT("Attribute Set"));

	// Create widget
	HealthWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Health Widget"));
	HealthWidget->SetupAttachment(RootComponent);
	HealthWidget->SetWidgetSpace(EWidgetSpace::Screen);
	HealthWidget->SetRelativeLocation(FVector(0, 0, 200));

	SelectionWidget = CreateDefaultSubobject<UWidgetComponent>(TEXT("Selection Widget"));
	SelectionWidget->SetupAttachment(RootComponent);
}

FGenericTeamId AEvilBuildingBase::GetGenericTeamId() const
{
	return TeamId;
}

BuildingType AEvilBuildingBase::GetBuildingType() const
{
	return Type;
}

void AEvilBuildingBase::Kill()
{
	AttributeSet->SetHealth(-1.0f);
}

void AEvilBuildingBase::BeginPlay()
{
	Super::BeginPlay();
	
	// Bind events
	AEvilGameModeBase* GameMode = (AEvilGameModeBase*)GetWorld()->GetAuthGameMode();
	GameMode->TimeStateChangedEvent.AddUObject(this, &AEvilBuildingBase::OnTimeStateChanged);

	// Add reference
	ULevelActorReferencesSubsystem* ReferenceSubsystem = GetWorld()->GetSubsystem<ULevelActorReferencesSubsystem>();
	if (ReferenceSubsystem)
		ReferenceSubsystem->AddSpawnedBuildingReference(this);
}

void AEvilBuildingBase::EndPlay(const EEndPlayReason::Type EndPlayReason)
{
	// Remove reference
	ULevelActorReferencesSubsystem* ReferenceSubsystem = GetWorld()->GetSubsystem<ULevelActorReferencesSubsystem>();
	if (ReferenceSubsystem)
		ReferenceSubsystem->RemoveBuilding(this);
}

void AEvilBuildingBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

UAbilitySystemComponent* AEvilBuildingBase::GetAbilitySystemComponent() const
{
	return AbilitySystemComponent;
}

float AEvilBuildingBase::GetHealth_Implementation() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetHealth();
	}
	return 0.f;
}

float AEvilBuildingBase::GetMaxHealth_Implementation() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetMaxHealth();
	}
	return 0.f;
}

float AEvilBuildingBase::GetAttackPower() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetAttackPower();
	}
	return 0.f;
}

float AEvilBuildingBase::GetDefensePower() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetDefensePower();
	}
	return 0.f;
}

float AEvilBuildingBase::GetFieldOfView() const
{
	if (IsValid(AttributeSet))
	{
		return AttributeSet->GetFieldOfView();
	}
	return 0.0f;
}

