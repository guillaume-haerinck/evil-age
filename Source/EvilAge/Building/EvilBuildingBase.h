// MIT License

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EvilTypes.h"
#include "Interfaces/DamageableInterface.h"
#include "AbilitySystemInterface.h"
#include "GenericTeamAgentInterface.h"
#include "EvilBuildingBase.generated.h"

/**
 * Offensive, passive and entrable buildings. Can be damaged and part of the ability system
 */
UCLASS()
class EVILAGE_API AEvilBuildingBase : public AActor, public IDamageableInterface, public IAbilitySystemInterface, public IGenericTeamAgentInterface
{
	GENERATED_BODY()
	
public:
	AEvilBuildingBase();
	virtual void Tick(float DeltaTime) override;

	// Implement IAbilitySystemInterface
	UAbilitySystemComponent* GetAbilitySystemComponent() const override;

	// Implement IDamageableInterface
	float GetHealth_Implementation() const override;
	float GetMaxHealth_Implementation() const override;

	UFUNCTION(BlueprintCallable)
	float GetFieldOfView() const;

	UFUNCTION(BlueprintCallable)
	float GetAttackPower() const;

	UFUNCTION(BlueprintCallable)
	float GetDefensePower() const;

	UFUNCTION(BlueprintCallable)
	BuildingType GetBuildingType() const;

	void Kill();

	// Implement IGenericTeamInterface 
	UFUNCTION(BlueprintCallable, Category = "AI")
	virtual FGenericTeamId GetGenericTeamId() const override;

protected:
	virtual void BeginPlay() override;
	virtual void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintImplementableEvent)
	void OnTimeStateChanged(TimeState timeState);

	UPROPERTY(EditDefaultsOnly, Category = "AI")
	FGenericTeamId TeamId;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	class UStaticMeshComponent* Mesh;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = "Health")
	class UWidgetComponent* HealthWidget;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	class UWidgetComponent* SelectionWidget;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities")
	class UEvilAbilitySystemComponent* AbilitySystemComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Abilities")
	class UEvilAttributeSet* AttributeSet;

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Abilities")
	BuildingType Type;
};
