// Copyright Epic Games, Inc. All Rights Reserved.

using UnrealBuildTool;

public class EvilAge : ModuleRules
{
	public EvilAge(ReadOnlyTargetRules Target) : base(Target)
	{
		PCHUsage = PCHUsageMode.UseExplicitOrSharedPCHs;

		PublicDependencyModuleNames.AddRange(new string[] { "Core", "CoreUObject", "Engine", "InputCore", "GameplayTasks" });

		// "NavigationSystem" and "AIModule" might be needed in the future

		PrivateDependencyModuleNames.AddRange(new string[] { "GameplayAbilities", "GameplayTags" });

		PublicIncludePaths.AddRange(new string[] { "EvilAge" });
	}
}
